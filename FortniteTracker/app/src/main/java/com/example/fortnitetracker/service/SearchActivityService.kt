package com.example.fortnitetracker.service

import android.content.Context
import com.example.fortnitetracker.mapper.UserData
import com.github.kittinunf.fuel.core.ResponseResultOf

interface SearchActivityService {
    fun fetchUserData(userName: String, platform: String): ResponseResultOf<String>

    fun storeInDatabase(userData: UserData)

    fun checkIfDataExists(userData: UserData, playerName: String, context: Context): Boolean
}