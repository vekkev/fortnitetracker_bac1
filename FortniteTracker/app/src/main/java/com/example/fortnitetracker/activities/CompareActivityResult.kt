package com.example.fortnitetracker.activities

import android.os.Bundle
import android.widget.EditText
import androidx.appcompat.app.AppCompatActivity
import com.example.fortnitetracker.R
import com.example.fortnitetracker.fields.UserDataFields
import com.example.fortnitetracker.mapper.UserData

class CompareActivityResult : AppCompatActivity() {

    private lateinit var statisticPlayer1: EditText
    private lateinit var statisticPlayer2: EditText

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_compare_result)

        statisticPlayer1 = findViewById(R.id.compare1)
        statisticPlayer2 = findViewById(R.id.compare2)


        val intent = intent
        val player1 = intent.getParcelableExtra<UserData>(UserDataFields.player1.name)
        val player2 = intent.getParcelableExtra<UserData>(UserDataFields.player2.name)
        fillViewWithData(player1, statisticPlayer1)
        fillViewWithData(player2, statisticPlayer2)
    }


    private fun fillViewWithData(userData: UserData, item: EditText) {
        item.append("Username: " + userData.epicUserHandle + "\n")
        item.append("Plattform: " + userData.platformNameLong + "\n")
        item.append("Top 5: " + userData.lifeTimeStats[0].value + "\n")
        item.append("Top 3: " + userData.lifeTimeStats[1].value + "\n")
        item.append("Top 6: " + userData.lifeTimeStats[2].value + "\n")
        item.append("Top 10: " + userData.lifeTimeStats[3].value + "\n")
        item.append("Top 12: " + userData.lifeTimeStats[4].value + "\n")
        item.append("Top 25: " + userData.lifeTimeStats[5].value + "\n")
        item.append("Score: " + userData.lifeTimeStats[6].value + "\n")
        item.append("Matches Played: " + userData.lifeTimeStats[7].value + "\n")
        item.append("Wins: " + userData.lifeTimeStats[8].value + "\n")
        item.append("Wins in %: " + userData.lifeTimeStats[9].value + "\n")
        item.append("Kills: " + userData.lifeTimeStats[10].value + "\n")
        item.append("K/D: " + userData.lifeTimeStats[11].value + "\n")
    }
}
