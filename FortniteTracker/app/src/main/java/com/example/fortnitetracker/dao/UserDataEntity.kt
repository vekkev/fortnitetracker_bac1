package com.example.fortnitetracker.dao

data class UserDataEntity(
    var id: Int = -1,
    var accountid: String ="",
    var epicUserHandle: String="",
    var platformNameLong: String="",
    var top5: String="",
    var top3: String="",
    var top6: String="",
    var top10: String="",
    var top12: String="",
    var top25: String="",
    var score: String="",
    var matches_played: String="",
    var wins: String="",
    var winsPer: String="",
    var kills: String="",
    var kd: String=""
) {

    companion object {
        val COLUMN_ID = "id"
        val TABLE_NAME = "UserStats"
        val COLUMN_ACCOUNTID = "accountid"
        val COLUMN_EPICUSERHANDLE = "epicuserhandle"
        val COLUMN_PATFORMNAMELONG = "platformnamelong"
        val COLUMN_TOP5 = "top5"
        val COLUMN_TOP3 = "top3"
        val COLUMN_TOP6 = "top6"
        val COLUMN_TOP10 = "top10"
        val COLUMN_TOP12 = "top12"
        val COLUMN_TOP25 = "top25"
        val COLUMN_SCORE = "score"
        val COLUMN_MATCHES_PLAYED = "matches_played"
        val COLUMN_WINS = "wins"
        val COLUMN_WINPER = "win_per"
        val COLUMN_KILLS = "kills"
        val COLUMN_KD = "kd"
    }
}