package com.example.fortnitetracker.activities

import android.os.Bundle
import android.widget.TextView
import androidx.appcompat.app.AppCompatActivity
import com.example.fortnitetracker.R
import com.example.fortnitetracker.dao.UserDataEntity
import com.example.fortnitetracker.dao.UserStatsDao
import com.example.fortnitetracker.fields.UserDataFields

class StatisticActivity : AppCompatActivity() {


    private lateinit var persInform: TextView
    private lateinit var ranking: TextView
    private lateinit var scores: TextView


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_statistic)

        
        persInform = findViewById(R.id.textViewTop)
        ranking = findViewById(R.id.textViewMiddle)
        scores = findViewById(R.id.textViewBottom)


        val intent = intent

        val username = intent.extras!!.getString(UserDataFields.username.name).toString()

        val database = UserStatsDao(this)
        val userData = database.getSingleUserData(username)

        fillViewWithData(userData)
    }

    private fun fillViewWithData(userData: UserDataEntity?) {
        persInform.append("Username: " + userData!!.epicUserHandle + "\n")
        persInform.append("Plattform: " + userData!!.platformNameLong + "\n")
        ranking.append("Top 5: " + userData!!.top5 + "\n")
        ranking.append("Top 3: " + userData!!.top3 + "\n")
        ranking.append("Top 6: " + userData!!.top6 + "\n")
        ranking.append("Top 10: " + userData!!.top10 + "\n")
        ranking.append("Top 12: " + userData!!.top12 + "\n")
        ranking.append("Top 25: " + userData!!.top25 + "\n")
        scores.append("Score: " + userData!!.score + "\n")
        scores.append("Matches Played: " + userData!!.matches_played + "\n")
        scores.append("Wins: " + userData!!.wins + "\n")
        scores.append("Wins in %: " + userData!!.winsPer + "\n")
        scores.append("Kills: " + userData!!.kills + "\n")
        scores.append("K/D: " + userData!!.kd + "\n")
    }
}