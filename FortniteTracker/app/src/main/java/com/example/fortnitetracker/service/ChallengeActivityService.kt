package com.example.fortnitetracker.service

import com.github.kittinunf.fuel.core.ResponseResultOf

interface ChallengeActivityService {

    fun fetchUserData() : ResponseResultOf<String>

}