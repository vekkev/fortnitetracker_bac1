package com.example.fortnitetracker.activities

import android.content.Intent
import android.os.Bundle
import android.os.StrictMode
import android.view.View
import android.widget.Button
import android.widget.EditText
import android.widget.Spinner
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.example.fortnitetracker.R
import com.example.fortnitetracker.dao.UserStatsDao
import com.example.fortnitetracker.fields.UserDataFields
import com.example.fortnitetracker.mapper.UserData
import com.example.fortnitetracker.service.SearchActivityService
import com.example.fortnitetracker.service.SearchActivityServiceImpl
import com.google.gson.Gson
import java.lang.RuntimeException

class SearchActivity : AppCompatActivity(), View.OnClickListener {

    //initialisation of widgets
    private lateinit var buttonSearch: Button
    private lateinit var usernameEditText: EditText
    private lateinit var platform: Spinner
    private lateinit var searchActivityService: SearchActivityService
    private lateinit var userStatsDao: UserStatsDao

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_search)

        //prevents app from deadlock
        val policy = StrictMode.ThreadPolicy.Builder().permitAll().build()
        StrictMode.setThreadPolicy(policy)

        //allocation of the widgets
        buttonSearch = findViewById(R.id.getStatsButton)
        usernameEditText = findViewById(R.id.accountName)
        platform = findViewById(R.id.platform)
        buttonSearch.setOnClickListener(this)
        userStatsDao = UserStatsDao(this)
        searchActivityService = SearchActivityServiceImpl(userStatsDao)
    }

    override fun onClick(v: View?) {
        val userName = usernameEditText.text.toString()

        try {
            val gson = Gson()
            val fetchUserData =
                searchActivityService.fetchUserData(userName, platform.selectedItem.toString())
            val userData = gson.fromJson(fetchUserData.third.component1(), UserData::class.java)

            if (performDataCheck(userData, userName)) {
                searchActivityService.storeInDatabase(userData)
                val intent = Intent(this, StatisticActivity::class.java)
                intent.putExtra(UserDataFields.username.name, userName.toLowerCase())
                startActivity(intent)
            }

        } catch (e: RuntimeException) {
            val intent = Intent(this, MainActivity::class.java)
            startActivity(intent)
            Toast.makeText(this, "Service currently unavailable!", Toast.LENGTH_LONG).show()
        }
    }

    private fun performDataCheck(
        userData: UserData,
        userName: String
    ) = searchActivityService.checkIfDataExists(userData, userName, this)
}
