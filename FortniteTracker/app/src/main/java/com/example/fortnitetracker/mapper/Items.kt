package com.example.fortnitetracker.mapper

import android.os.Parcelable
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize

@Parcelize
data class Items(val metadata: List<MetaData>) : Parcelable {
    class Deserializer : ResponseDeserializable<Items> {
        override fun deserialize(content: String): Items = Gson().fromJson(content, Items::class.java)
    }
}