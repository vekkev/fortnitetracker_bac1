package com.example.fortnitetracker.dao

import android.content.ContentValues
import android.content.Context
import android.database.sqlite.SQLiteDatabase
import com.example.fortnitetracker.mapper.LifeTimeStats
import org.jetbrains.anko.db.*

class UserStatsDao(ctx: Context) : ManagedSQLiteOpenHelper(ctx, "UserStats", null, 1) {
    private val TABLE_NAME = "UserStats"
    private val COL1 = "id"
    private val COL2 = "accountid"
    private val COL3 = "epicUserHandle"
    private val COL4 = "platformNameLong"
    private val COL5 = "top5"
    private val COL6 = "top3"
    private val COL7 = "top6"
    private val COL8 = "top10"
    private val COL9 = "top12"
    private val COL10 = "top25"
    private val COL11 = "score"
    private val COL12 = "matches_played"
    private val COL13 = "wins"
    private val COL14 = "win_per"
    private val COL15 = "kills"
    private val COL16 = "kd"

    companion object {
        private var instance: UserStatsDao? = null

        @Synchronized
        fun Instance(context: Context): UserStatsDao {
            if (instance == null) {
                instance =
                    UserStatsDao(context.applicationContext)
            }
            return instance!!
        }
    }

    override fun onCreate(db: SQLiteDatabase?) {
        db?.createTable(
            TABLE_NAME, true,
            COL1 to INTEGER + PRIMARY_KEY + UNIQUE,
            COL2 to TEXT,
            COL3 to TEXT,
            COL4 to TEXT,
            COL5 to TEXT,
            COL6 to TEXT,
            COL7 to TEXT,
            COL8 to TEXT,
            COL9 to TEXT,
            COL10 to TEXT,
            COL11 to TEXT,
            COL12 to TEXT,
            COL13 to TEXT,
            COL14 to TEXT,
            COL15 to TEXT,
            COL16 to TEXT
        )
    }

    override fun onUpgrade(db: SQLiteDatabase?, p1: Int, p2: Int) {
        db?.dropTable(TABLE_NAME, ifExists = true)
    }

    fun addData(
        accountId: String,
        platformName: String,
        epicUserHandle: String,
        lifeTimeStatMappers: List<LifeTimeStats>
    ) {
        val db: SQLiteDatabase = this.writableDatabase
        val values = ContentValues()
        values.put(COL2, accountId)
        values.put(COL3, epicUserHandle.toLowerCase())
        values.put(COL4, platformName)
        values.put(COL5, lifeTimeStatMappers[0].value)
        values.put(COL6, lifeTimeStatMappers[1].value)
        values.put(COL7, lifeTimeStatMappers[2].value)
        values.put(COL8, lifeTimeStatMappers[3].value)
        values.put(COL9, lifeTimeStatMappers[4].value)
        values.put(COL10, lifeTimeStatMappers[5].value)
        values.put(COL11, lifeTimeStatMappers[6].value)
        values.put(COL12, lifeTimeStatMappers[7].value)
        values.put(COL13, lifeTimeStatMappers[8].value)
        values.put(COL14, lifeTimeStatMappers[9].value)
        values.put(COL15, lifeTimeStatMappers[10].value)
        values.put(COL16, lifeTimeStatMappers[11].value)
        db.insert(TABLE_NAME, null, values)
    }




    fun getSingleUserData(user: String?): UserDataEntity? {
        val db: SQLiteDatabase = this.writableDatabase
        val selectQuery = "SELECT * FROM $TABLE_NAME WHERE $COL3 = ?"
        db.rawQuery(selectQuery, arrayOf(user)).use {
            // .use requires API 16
            if (it.moveToFirst()) {
                val result = UserDataEntity()
                result.id = it.getInt(it.getColumnIndex(COL1))
                result.accountid = it.getString(it.getColumnIndex(COL2))
                result.epicUserHandle = it.getString(it.getColumnIndex(COL3))
                result.platformNameLong = it.getString(it.getColumnIndex(COL4))
                result.top5 = it.getString(it.getColumnIndex(COL5))
                result.top3 = it.getString(it.getColumnIndex(COL6))
                result.top6 = it.getString(it.getColumnIndex(COL7))
                result.top10 = it.getString(it.getColumnIndex(COL8))
                result.top12 = it.getString(it.getColumnIndex(COL9))
                result.top25 = it.getString(it.getColumnIndex(COL10))
                result.score = it.getString(it.getColumnIndex(COL11))
                result.matches_played = it.getString(it.getColumnIndex(COL12))
                result.wins = it.getString(it.getColumnIndex(COL13))
                result.winsPer = it.getString(it.getColumnIndex(COL14))
                result.kills = it.getString(it.getColumnIndex(COL15))
                result.kd = it.getString(it.getColumnIndex(COL16))
                return result
            }
        }
        return null
    }
}