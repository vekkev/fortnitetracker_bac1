package com.example.fortnitetracker.dao

data class ChallengeDataDao(

    var type: String = "",
    var name: String = "",
    var complete: String = "",
    var total: String = "",
    var picture: String = "",
    var rewardName: String = "",
    var rewardDescription: String = ""
) {

    companion object {

        val COLUMN_TYPE = "type"
        val COLUMN_NAME = "name"
        val COLUMN_COMPLETE = "complete"
        val COLUMN_TOTAL = "total"
        val COLUMN_PICTURE = "picture"
        val COLUMN_REWARDNAME = "rewardName"
        val COLUMN_REWARDDESCRIPTION = "rewardDescription"


    }

}