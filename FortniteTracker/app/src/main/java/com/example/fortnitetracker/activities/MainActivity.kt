package com.example.fortnitetracker.activities

import android.content.Intent
import android.os.Bundle
import android.view.View
import android.widget.Button
import androidx.appcompat.app.AppCompatActivity
import com.example.fortnitetracker.R

class MainActivity : AppCompatActivity() {

    private lateinit var searchButton: Button
    private lateinit var comparisionButton: Button
    private lateinit var challengesButton: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        searchButton = findViewById(R.id.buttonSearch)
        comparisionButton = findViewById(R.id.buttonCompare)

        challengesButton = findViewById(R.id.buttonChallenges)
        challengesButton.setOnClickListener(challengeClickListener())


        searchButton.setOnClickListener(searchClickListener())
        comparisionButton.setOnClickListener(comparisonClickListener())

    }

    private fun comparisonClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this, CompareActivity::class.java)
            startActivity(intent)
        }
    }

    private fun searchClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this, SearchActivity::class.java)
            startActivity(intent)
        }
    }

    private fun challengeClickListener(): View.OnClickListener {
        return View.OnClickListener {
            val intent = Intent(this, ChallengeActivity::class.java)
            startActivity(intent)

        }

    }
}
