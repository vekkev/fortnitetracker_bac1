package com.example.fortnitetracker.fields

enum class UserDataFields {
    player1,
    player2,
    username
}