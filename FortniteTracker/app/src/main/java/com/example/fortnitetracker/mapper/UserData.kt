package com.example.fortnitetracker.mapper

import android.os.Parcelable
import com.github.kittinunf.fuel.core.ResponseDeserializable
import com.google.gson.Gson
import kotlinx.android.parcel.Parcelize

@Parcelize
data class UserData(
    val accountId: String = "",
    val platformNameLong: String = "",
    val epicUserHandle: String = "",
    val lifeTimeStats: List<LifeTimeStats>
) : Parcelable {
    class Deserializer : ResponseDeserializable<UserData> {
        override fun deserialize(content: String): UserData = Gson().fromJson(content, UserData::class.java)
    }
}
